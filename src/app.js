const express = require('express');
const bodyParser = require('body-parser');
const {sequelize, Contract} = require('./model')
const {getProfile} = require('./middleware/getProfile')
const {where, Op, QueryTypes} = require("sequelize");
const {parse} = require("nodemon/lib/cli");
const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize)
app.set('models', sequelize.models)

/**
 * FIX ME!
 * @returns contract by id
 */
app.get('/contracts/:id', getProfile, async (req, res) => {
    const {Contract} = req.app.get('models')
    const {id} = req.params

    const contract = await Contract.findOne({
        where: {
            id,
            clientId: req.profile.id
        }
    })
    if (!contract) return res.status(404).json({ message: 'Not found' })
    res.json(contract)
})

app.get('/contracts', getProfile, async (req, res) => {
    try {
        const {Contract} = req.app.get('models');
        const whereConditions = {
            status: ['new', 'in_progress'],
        }
        if (req.profile.type === 'client') {
            whereConditions.ClientId = req.profile.id;
        } else {
            whereConditions.ContractorId = req.profile.id;
        }
        const data = await Contract.findAll({
            where: whereConditions,
            order: [
                ['createdAt', 'desc']
            ]
        });
        res.json(data)
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
});

app.get('/jobs/unpaid', getProfile, async (req, res) => {
    try {
        const {Job} = req.app.get('models');
        const whereConditions = {};
        if (req.profile.type === 'client') {
            whereConditions.ClientId = req.profile.id;
        } else {
            whereConditions.ContractorId = req.profile.id;
        }
        whereConditions.status = 'in_progress';
        const data = await Job.findAll({
            include: {
                model: Contract,
                where: whereConditions
            }
        })
        res.json(data)
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
})


app.post('/jobs/:job_id/pay', getProfile, async (req, res) => {
    try {
        const {
            Profile,
            Contract,
            Job
        } = req.app.get('models');
        const {job_id: jobId} = req.params;
        const job = await Job.findOne({
            where: {
                id: jobId
            }
        });

        if (!job) {
            return res.status(400).send({message: 'Invalid job id'})
        }
        if (job.paid) {
            return res.status(400).send({message: 'Already paid for this job'})
        }

        const contract = await Contract.findOne({
            where: {id: job.ContractId}
        });

        if (!contract) {
            return res.status(400).send({message: 'No contract found for this job'})
        }

        if (contract.ClientId !== req.profile.id || contract.status === 'terminated')
            return res.status(400).json({message: 'Invalid request'})

        const client = await Profile.findOne({
            where: {id: contract.ClientId}
        })

        if (client.balance < job.price)
            return res.status(400).json({message: 'Insufficient balance'});

        const trx = await sequelize.transaction();

        try {
            await Profile.increment('balance', {by: job.price, where: {id: contract.ContractorId}}, {transaction: trx});
            await Profile.increment('balance', {by: -job.price, where: {id: contract.ClientId}}, {transaction: trx});
            await Job.update({paid: true, paymentDate: new Date()}, {where: {id: jobId}, transaction: trx});
            await Contract.update({status: 'terminated'}, {where: {id: job.ContractId}, transaction: trx})
            await trx.commit();
            res.json({message: 'Success'})
        } catch (err) {
            console.error(err);
            await trx.rollback()
            res.status(500).send({message: err.message})
        }
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
})


app.post('/balances/deposit/:userId', getProfile, async (req, res) => {
    try {
        const {Profile, Contract, Job} = req.app.get('models');
        const {userId} = req.params;
        const {amount} = req.body;

        if(parseInt(userId) !== req.profile.id)
            return res.status(400).json({message: 'Invalid request'});

        const {dataValues: client} = await Profile.findOne({
            where: {id: userId, type: 'client'}
        });

        if (!client || (client && client.id !== parseInt(userId)))
            return res.status(400).json({message: 'Invalid request'});

        const contracts = await Contract.findAll({
            where: {
                clientId: userId,
                status: ['new', 'in_progress'],
            },
            include: {
                model: Job,
                where: {
                    [Op.or]: [
                        {paid: null},
                        {paid: false}
                    ]
                }
            }
        })

        if (!contracts)
            return res.status(400).json({message: 'There are no active contracts'});

        let total = 0;

        for (const contract of contracts) {

            const contractTotal = contract.Jobs.reduce((acc, job) => {
                acc += job.price;
                return acc
            }, 0);
            total += contractTotal
        }
        const maxBalanceToAdd = total * 0.25;
        if (amount > maxBalanceToAdd)
            return res.status(400).json({message: 'Cannot add more than ' + maxBalanceToAdd});

        await Profile.increment('balance', {by: amount, where: {id: userId}});
        res.json({message: 'Success'})
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
})

app.get('/admin/best-profession', async (req, res) => {
    try {
        let {start, end} = req.query;
        const {Job} = req.app.get('models');

        const highestPaidJob = await Job.findOne({
            where: {
                paid: true,
                createdAt: {
                    [Op.between]: [start, end]
                },
                paymentDate: {
                    [Op.between]: [start, end]
                }
            },
            order: [
                ['price', 'desc']
            ],
            limit: 1
        })
        res.json(highestPaidJob)
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
})

app.get('/admin/best-clients', async (req, res) => {
    try {
        let {start, end, limit} = req.query;
        if(!limit) limit = 3;

        const data = await sequelize.query(`
            SELECT profiles.id, (profiles.firstName || ' ' || profiles.lastName) as fullName, SUM(jobs.price) as "paid"
            from profiles
                 JOIN contracts ON contracts.ClientId = profiles.id
                 JOIN jobs ON jobs.ContractId = contracts.id
            where 
                profiles.type = 'client'
                and jobs.paid = TRUE 
                and jobs.createdAt between DATETIME('${start}') and DATETIME('${end}') 
                and jobs.paymentDate between DATETIME('${start}') and DATETIME('${end}')
            group by profiles.id
            order by paid desc
            limit ${limit}`,
            {type: QueryTypes.SELECT}
        )

        res.json(data)
    } catch (err) {
        console.error(err);
        res.status(500).send({message: err.message})
    }
})

module.exports = app;
